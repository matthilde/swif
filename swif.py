#!/usr/bin/env python3
#
# Web-scrapping tool that extracts schedules from a short-wave.info
# query.
#
import requests, sys
from datetime import datetime
from bs4 import BeautifulSoup
from pprint import pprint
from dataclasses import dataclass

_current_date = datetime.utcnow()
_current_time = '%02d%02d' % (_current_date.hour, _current_date.minute)
def in_schedule(start, end, days):
    start = int(start)
    end   = int(end)
    cur   = int(_current_time)
    day   = _current_date.isoweekday()

    if days[day%7] == '.': return False
    if start < end:
        return cur in range(start, end+1)
    elif start == end:
        return True
    else:
        return cur not in range(end, start+1)

@dataclass
class QueryEntry:
    freq: int
    station: str
    start: str
    end: str
    days: str
    langs: list
    power: float
    country: str
    location: str
    signal: str
_categories = ['S', 'FREQ', 'NAME', 'STRT', 'END', 'DAYS',
                'LANGS', 'PW', 'C', 'LOC']
_catattr = {'S': 'signal',
            'FREQ': 'freq',
            'NAME': 'station',
            'STRT': 'start',
            'END': 'end',
            'DAYS': 'days',
            'LANGS': 'langs',
            'PW': 'power',
            'C': 'country',
            'LOC': 'location'}
    
def do_request(**data):
    return requests.post("https://short-wave.info/index.php",
                         headers = {
                             "accept": "*/*",
                             "User-Agent": "swif/1.0"
                         }, data = data)

def parse_days(daysched):
    if len(daysched) != 7: return "-------"
    days = 'SMTWTFS'
    return ''.join([days[i] if x != '.' else '.' for i, x in
                    enumerate(daysched)])
def parse_signal(sig):
    if sig == 'OFF_AIR':
        return '-'
    elif sig.startswith("SIGNAL_STRENGTH_"):
        return sig[-1]
    else:
        return '?'
    
def parse_schedule(html):
    soup = BeautifulSoup(html, "html.parser")
    table = soup.find(id = 'output')
    assert table != None, "Table not found."

    table = table.tbody
    assert table != None, "Table body missing."
    # print(list(table.stripped_strings))

    # pprint([list(x.stripped_strings) for x in table.find_all('tr')])
    d = [[list(y.stripped_strings) for y in x.find_all('td')]
         for x in table.find_all('tr')]
    result = []
    for entry in d:
        # pprint(entry)
        result.append(QueryEntry(
            freq = int(entry[0][0]),
            station = ' '.join(entry[1]),
            start = entry[2][0].replace(':', ''),
            end = entry[3][0].replace(':', ''),
            days = parse_days(entry[4][0]),
            langs = [x for x in entry[5] if x != ','],
            power = float(entry[6][0] if len(entry[6]) > 0 else 0),
            country = entry[8][0] if len(entry[8]) > 0 else "???",
            location = entry[8][1] if len(entry[8]) > 1 else "???",
            signal = parse_signal(entry[9][0]))) if len(entry[9]) > 0 else "?"

    return result

tostr = lambda n: ','.join(n) if type(n) is list else str(n)
def get_max_length(query, attr):
    return max(len(tostr(x.__getattribute__(attr))) for x in query)+1

def print_query(query, cats = _categories, only_onair = False):
    lens = {
        "S": 2,
        "FREQ": 6,
        "NAME": get_max_length(query, "station"),
        "STRT": 5,
        "END": 5,
        "DAYS": 8,
        "LANGS": get_max_length(query, "langs"),
        "PW": 7,
        "C": 4,
        "LOC": get_max_length(query, "location")
    }

    # Print header
    print('\033[1;7m', end='')
    for cat in _categories:
        # To make sure it's in the right order.
        if cat not in cats: continue
        print(cat + ' '*(lens[cat]-len(cat)), end='')
    print('\033[0m')

    # Print entries
    for entry in query:
        x = int(in_schedule(entry.start, entry.end, entry.days))
        if x == 0 and only_onair: continue
        print(f"\033[{x}m", end='')
        for cat in _categories:
            if cat not in cats: continue
            n = tostr(entry.__getattribute__(_catattr[cat]))
            print(n + ' '*(lens[cat]-len(n)), end='')
        print()

def print_query_csv(query, only_onair = False):
    print(','.join(_categories))
    for entry in query:
        x = int(in_schedule(entry.start, entry.end, entry.days))
        if x == 0 and only_onair: continue
        n = ','.join(
            '"'+tostr(entry.__getattribute__(_catattr[cat]))+'"'
            for cat in _categories)
        print(n)

######################################################################
######################################################################

def show_help():
    print("""swif - CLI tool to get short-wave.info schedules
    -f FREQ     - Query at specific frequency
    -l LANG     - Query specific language
    -r STATION  - Query specific station
    -t TIME     - Query at time in UTC (HHMM 24-hour format)
    -b LENGTH   - Query at a specific band
    -o          - Only show on-air stations
    -c          - Print results in CSV format

Allowed bands (meters):
    120, 90, 75, 60, 49, 41, 31, 25, 22, 19, 16, 15, 13

Examples:
    swif -f 3955
    swif -l English -r BBC -o
    swif -l French -t 1600 -b 49
""", end='')
    sys.exit(1)

def parse_args():
    global _current_time
    
    data = {}
    onlyon = False
    printfn = print_query
    argpop = lambda: show_help() if args == [] else args.pop(0)
    args = sys.argv.copy()[1:]
    while args != []:
        opt = argpop()
        if len(opt) != 2 or opt[0] != '-': show_help()
        opt = opt[1]
        if opt == 'o':
            onlyon = True
            continue
        elif opt == 'c':
            printfn = print_query_csv
            continue
        elif opt not in 'flrtb': show_help()

        a = argpop()
        if opt == 'f':
            if not a.isdigit(): show_help()
            data['freq'] = a
        elif opt == 'l':
            data['language'] = a.title()
        elif opt == 'r':
            data['station'] = a
        elif opt == 't':
            if not (len(a) == 4 and a.isdigit()): show_help()
            data['now'] = str(int(a[:2]))
            data['now2'] = str(int(a[2:]))
            _current_time = a
        elif opt == 'b':
            if a not in ['120', '90', '75', '60', '49',
                         '41', '31', '25', '22', '19',
                         '16', '15', '13']: show_help()
            data['band'] = a + 'm'
        else:
            show_help()

    return printfn, onlyon, data

if __name__ == "__main__":
    if len(sys.argv) < 3: show_help()

    printfn, onlyon, data = parse_args()
    try:
        r = do_request(**data)
        q = parse_schedule(r.text)
    except AssertionError as e:
        if str(e) == "Table not found.":
            sys.exit(1)
        else:
            raise e
    else:
        printfn(q, only_onair = onlyon)
