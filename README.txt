              _  __ 
 _____      _(_)/ _|
/ __\ \ /\ / / | |_  short-wave.info CLI tool
\__ \\ V  V /| |  _| by matthilde
|___/ \_/\_/ |_|_|  

swif is a script written in Python that uses the magic of web scrapping to
extract the data from a short-wave.info query. It then makes an awesome CLI tool
to get query results directly from the comfort of your terminal!

DEPENDENCIES
------------
All you need to install to get it to work is
 * requests
 * BeautifulSoup4

HOW TO USE
----------
swif OPTIONS...

    -f FREQ     - Query at specific frequency
    -l LANG     - Query specific language
    -r STATION  - Query specific station
    -o          - Only show on-air stations
    -t TIME     - Query at time in UTC (HHMM 24-hour format)
    -b LENGTH   - Query at a specific band

Allowed bands (meters):
    120, 90, 75, 60, 49, 41, 31, 25, 22, 19, 16, 15, 13

Examples:
    swif -f 3955
    swif -l English -r BBC -o
    swif -l French -t 1600 -b 49

LICENSING
---------
This tool is licensed under the MIT License.
